var mysql = require('mysql'),

/*connection = mysql.createConnection( 		// configuration to the database
	{ 
		host: 'localhost', 
		user: 'root',  
		password: 'root', 
		database: 'tango',
		port: '8889'
	}
);*/

connection = mysql.createConnection( 		// configuration to the database
	{ 
		host: '162.243.25.181', 
		user: 'kpcit',  
		password: '@dmin_2017', 
		database: 'tango',
		port: '3306'
	}
);

var carModel = {};

carModel.getCars = function(callback){						// function to get all cars
	if (connection){
		connection.query('SELECT * FROM cars ORDER BY id', function(error, rows) {
			if(error){
				throw error;
			}else{
				callback(null, rows);
			}
		});
	}
}

carModel.getCar = function(id,callback){						// function to get a car by Id 
	if (connection){
		var sql = 'SELECT * FROM cars WHERE id = ' + connection.escape(id);
		connection.query(sql, function(error, row){
			if(error){
				throw error;
			}else{
				callback(null, row);
			}
		});
	}
}

carModel.getImagesByCarId = function(id, callback){				// function to get images of a car by Id
	if (connection){
		var sql = 'SELECT * FROM images WHERE id_car = ' + connection.escape(id);
		connection.query(sql, function(error, row){
			if(error){
				throw error;
			}else{
				callback(null, row);
			}
		});
	}
}

carModel.getFeaturesByCarId = function(id, callback){			// function to get a featrures of a car by Id
	if (connection){
		var sql = 'SELECT * FROM features WHERE id_car = ' + connection.escape(id);
		connection.query(sql, function(error, row){
			if(error){
				throw error;
			}else{
				callback(null, row);
			}
		});
	}
}

module.exports = carModel;			