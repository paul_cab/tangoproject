var UserModel = require('../models/apiModel');				// Reference to the model

module.exports = function(app){							
	
	app.get("/cars", function(req,res){						// service to get all cars
		UserModel.getCars(function(error, data){
			res.json(200,data);
		});
	});

	app.get("/cars/:id", function(req,res){					// service to get a car by Id
		var id = req.params.id;
		if(!isNaN(id)){
			UserModel.getCar(id,function(error, objCar){
				if (typeof objCar !== 'undefined' && objCar.length > 0){	// if exist return code 200 and data
					UserModel.getImagesByCarId(id, function(error, objImages){
						if (typeof objImages !== 'undefined' && objImages.length > 0){	// if exist return code 200 and data
							objCar[0].images = objImages;
							UserModel.getFeaturesByCarId(id, function(error, objFeatures){
								if (typeof objFeatures !== 'undefined' && objFeatures.length > 0){	// if exist return code 200 and data
									objCar[0].features = objFeatures;
									res.json(200,objCar);	
								}else{
									res.json(404,{"msg":"features not found"});				// if id car not found
								}
							});
						}else{
							res.json(404,{"msg":"images not found"});				// if id car not found
						}
					});
				}else{
					res.json(404,{"msg":"car not found"});				// if id car not found
				}
			});
		}else{
			res.json(500,{"msg":"Id error"});							// if id is not a number
		}
	});	
}