import axios from 'axios'; 

export const SHOW_CARS = 'SHOW_CARS';

function URLConexion(url, method){					// Function to get a URL conexion
	return (dispath, getState) => {
		axios({
			method: method,
			url: url
		},{ crossdomain: true })
		.then((response) => {
			console.log(response);
			dispath( { type: SHOW_CARS, payload: response.data })
		});
	}
}

export function showCars(){							// Get a list of cars
	return (dispath, getState) => {
		axios({
			method: 'get',
			url: 'http://162.243.25.181:3000/cars'
		},{ crossdomain: true })
		.then((response) => {
			console.log(response);
			dispath( { type: SHOW_CARS, payload: response.data })
		});
	}
}

export function showCarsById(id){					// Get the car detail by Id car
	return (dispath, getState) => {
		axios({
			method: 'get',
			url: 'http://162.243.25.181:3000/cars/' + id
		},{ crossdomain: true })
		.then((response) => {
			console.log(response);
			dispath( { type: SHOW_CARS, payload: response.data })
		});
	}
}