import React, { Component } from 'react';

export default class Call_us extends Component {		// Component to show the Call Us Button

	render() {    
		return (
			<div className="row visible-xs">
	    		<div className="col-xs-12 ">
	    			<button type="button" className="btn btn-lg btn-success btn-call-us">CALL US</button>
	    		</div>
	    	</div>
		)
	}
}