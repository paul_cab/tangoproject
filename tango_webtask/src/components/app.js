import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import { showCars } from '../actions';
import { showCarsById } from '../actions';
import { Table } from 'react-bootstrap';
import Navbar from './navbar';
import Pictures from './pictures';
import Detail from './detail';
import Call_us from './call_us';
import Features from './features';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

class App extends Component {							// Main component

	constructor(props) {
		super(props);
	}

	componentDidMount() {								// Function used to get data from webservice
		if(this.idCar){
			this.props.showCarsById(this.idCar); 	
		}
	}

  	render() {
  		const CarDetail = ({ match }) => (									// When path contains a URL parameter
			<div>
				<div className="hidden">{this.idCar = match.params.id}</div>
				<Navbar />
				<Pictures />
				<div>
					<Detail />
					<Call_us />
					<Features />
				</div>
				<footer className="visible-xs">
			    	<div className="footer-text">
			    		<span> <a href="#">About US</a> / <a href="#">Terms</a> / <a href="#">Privacy Police</a> </span>
			    	</div>
			    	<div className="footer-text">
		    			<span>PLS is a registered service mark and other marks are service marks of PLS Financial Services, Inc. © 2016</span>
		    		</div>
			    </footer>
			    
			</div>
		)
		const Home = () => (												// When path contains only '/''
			<div>
				<a href="/1">Ford Focus</a>
				<br /><br />
				<a href="/2">Hummer H2</a>
			</div>
		)
	    return (
	    	<Router>
	    		<div>
		        	<Route exact path="/" component={Home}/>
			      	<Route path="/:id" component={CarDetail}/>
			  	</div>
		  	</Router>
	    )
 	}
}

function mapStateToProps(state){
	return {
		cars: state.car.list
	}
}

export default connect(mapStateToProps, { showCarsById })(App)