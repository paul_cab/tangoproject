import React, { Component } from 'react';

export default class Logo extends Component {					// Component to show navbar information

  	render() {

 	    return (
	      	<nav className="navbar navbar-static-top">
				<div className="container-fluid">
						<div id="navbar" className="">
						<ul className="nav navbar-nav navbar-right">
				          	<li className="menu-item active hidden-xs"><img className="icon" src="./images/search.png" /></li>
				          	<li className="menu-item"><img className="icon" src="./images/location.png" /></li>
				          	<li className="menu-item"><img className="icon" src="./images/telephone.png" /></li>
				          	<li className="menu-item hidden-xs"><img className="icon" src="./images/clock.png" /></li>
						</ul>
						</div>
				</div>
			</nav>
	    )
  	}
}