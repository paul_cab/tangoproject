import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showCarsById } from '../actions';

class Features extends Component {								// Component to show Features of the car

	

	renderImagesList(){
		return this.props.cars.map((car) => {
			return car.features.map((fea) => {							// Map the pictures of the object cars
				return (
					<div className="col-md-6 features" key={fea.id}>
		    			<table className="table">
				            <thead>
				              	<tr>
				                	<th>{fea.feature_name}</th>
				                	<th></th>
				              	</tr>
				            </thead>
				            <tbody>
				              	<tr>
				                	<td>Cylinders</td>
				                	<td>{fea.cylinders}</td>
				              	</tr>
				              	<tr>
				                	<td>City MPG</td>
				                	<td>{fea.city_mpg}</td>
				              	</tr>
				              	<tr>
				                	<td>Highway MPG</td>
				                	<td>{fea.highway_mpg}</td>
				              	</tr>
				              	<tr>
				                	<td>Engine</td>
				                	<td>{fea.engine}</td>
				              	</tr>
				            </tbody>
				        </table>	    			
		    		</div>
				)
			});
		});
	}

	render() {    																// Main function for render
		return (
			<div className="row top-space features-content">
	    		{this.renderImagesList()}
	    	</div>
		)
	}
}

function mapStateToProps(state){
	return {
		cars: state.car.list
	}
}

export default connect(mapStateToProps, { showCarsById })(Features)