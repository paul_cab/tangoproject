import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showCarsById } from '../actions';

class Detail extends Component {									// Component to show car details

  	constructor(props) {
    	super(props);
    	this.state = {
			imgSrc: ''
    	}
    	if(this.props.cars.length>0){										// Set the main picture of car
    		this.url_image = this.props.cars[0].images[0].url_image;
    		this.state = {
				imgSrc: './images/' + this.url_image
	    	}
    	}
    	
    	this.handleOnClick = this.handleOnClick.bind(this);
  	}

  	handleOnClick(url_image) {										// Handle onClick to change main picture of car
    	this.setState({
      		imgSrc: './images/' + url_image
    	});
  	}

	renderCarsList(){												// Function to render Car information details
		const style_H4 = {
			color: '#343434'
		}
		const style_H5 = {
			color: '#9B9B9B'
		}

		const style_Float = {
			float: 'left'
		}

		return this.props.cars.map((car) => {						// Map the cars object
			return (
				<div key={car.id}>
					<div className="col-md-9 hidden-xs car-image-container">
						<img height={432} width={728} src={this.state.imgSrc}/>
					</div>
					<div className="title">
						<h1>{car.brand} {car.model}</h1>
					</div>
					<div className="col-md-3 col-xs-6 car-detail-container">
						<div className="list-group">
				            <div className="list-group-item">
				              	<h5 style={style_H5}>Year</h5>
				              	<h4 style={style_H4}>{car.year}</h4>
				            </div>
				            <div className="list-group-item">
				              	<h5 style={style_H5}>Price Range</h5>
				              	<h4 style={style_H4}>{car.range_prices}</h4>
				            </div>
				            <div className="list-group-item">
				              	<h5 style={style_H5}>Mileage</h5>
				              	<h4 style={style_H4}>{car.mileage}</h4>
				            </div>
				        </div>
				    </div>
				    <div className="col-md-3 col-xs-6 car-detail-container">
				        <div className="list-group">
				            <div className="list-group-item">
				              	<h5 style={style_H5}>Item number: #{car.item_number}</h5>
				              	<h5 style={style_H5}>VIN: {car.vin}</h5>
				            </div>
				            <div className="list-group-item top15">
				            	<span>
									<h5 className="list-group-item-heading float-left">Share this car</h5>
				              		<img className="icon icon-email" src="./images/email.png" />
				            	</span>
				              	
				            </div>
				            <div className="list-group-item top15">
				            	<ul className="list-inline">
								  	<li><h5 className="list-group-item-heading">Views</h5><span className="numbers">{car.views}</span></li>
								  	<li className="hidden-xs"><h5 className="list-group-item-heading">Saves</h5><span className="numbers">{car.saves}</span></li>
								  	<li className="hidden-xs"><h5 className="list-group-item-heading ">Shares</h5><span className="numbers">{car.shares}</span></li>
								</ul>
				            </div>
				        </div>
					</div>
				</div>
			)
		});
	}

	renderImagesList(){											// Function to render the car pictures

		return this.props.cars.map((car) => {
			return car.images.map((img) => {
				this.url_image = './images/' + img.url_image;
				const imgBackground = {
					 backgroundImage: "url('" + this.url_image + "')"
				}
				return (
					<div key={img.id} onClick={this.handleOnClick.bind(null,img.url_image)} className="col-md-2 thumbail" style={imgBackground}>
						
					</div>	
				)
			});
		});
	}

  	render() {													// Main render of the component
	    return (
  			<div>  				
			    <div className="row">
					{this.renderCarsList()}
				</div>
				<div className="row color-gray hidden-xs">
					{this.renderImagesList()}
		    	</div>
	    	</div>
	    )
  	}
}

function mapStateToProps(state){
	return {
		cars: state.car.list
	}
}

export default connect(mapStateToProps, { showCarsById })(Detail)
