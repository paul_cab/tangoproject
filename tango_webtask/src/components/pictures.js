import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showCarsById } from '../actions';
import { Carousel } from 'react-bootstrap';

class Pictures extends Component {						// Component to show carousel when mobile view

	

	renderImagesList(){

		return this.props.cars.map((car) => {
			return car.images.map((img) => {
				this.url_image = './images/' + img.url_image;
				return (
					<Carousel.Item height={250} key={img.id}>
			          <img width={900} height={500} src={this.url_image}/>
			        </Carousel.Item>
				)
			});
		});
	}

  	render() {
	    return (
	    	<Carousel height={250} className="visible-xs">				// Declare Carousel object from react-bootstrap
	    		{this.renderImagesList()}
		    </Carousel>
	    )
  	}
}

function mapStateToProps(state){
	return {
		cars: state.car.list
	}
}

export default connect(mapStateToProps, { showCarsById })(Pictures)
