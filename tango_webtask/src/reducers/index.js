import { combineReducers } from 'redux';
import { showCars } from './cars';

const rootReducer = combineReducers({
  car: showCars
});

export default rootReducer;
