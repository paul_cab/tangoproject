import { SHOW_CARS } from '../actions'
const initialState = {
	list: []
}

export function showCars(state = initialState, action) { 				// Function to reduce Cars object
	switch (action.type){
		case SHOW_CARS: 
			return Object.assign({}, state, {list: action.payload})
		default:
			return state
	}
}

export function showCarsById(state = initialState, action) {			// Function to reduce Cars object by Id
	switch (action.type){
		case SHOW_CARS: 
			return Object.assign({}, state, {list: action.payload})
		default:
			return state
	}
}