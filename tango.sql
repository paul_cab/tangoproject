# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 162.243.25.181 (MySQL 5.5.46-0ubuntu0.14.04.2)
# Base de datos: tango
# Tiempo de Generación: 2017-09-05 23:38:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla cars
# ------------------------------------------------------------

CREATE TABLE `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand` varchar(20) DEFAULT NULL,
  `model` varchar(20) DEFAULT NULL,
  `year` varchar(4) DEFAULT NULL,
  `range_prices` varchar(20) DEFAULT NULL,
  `mileage` varchar(20) DEFAULT NULL,
  `item_number` varchar(20) DEFAULT NULL,
  `vin` varchar(20) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `shares` int(11) DEFAULT NULL,
  `saves` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cars` WRITE;
/*!40000 ALTER TABLE `cars` DISABLE KEYS */;

INSERT INTO `cars` (`id`, `brand`, `model`, `year`, `range_prices`, `mileage`, `item_number`, `vin`, `views`, `shares`, `saves`)
VALUES
	(1,'Ford','Focus','2012','8.500 - 9.000','200.000 miles','1395P','3GNDA13D96S6314',324,32,12),
	(2,'Hummer','H2','2011','18300','120.200 miles','1234P','3GNDA13D96S6355',123,23,21);

/*!40000 ALTER TABLE `cars` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla features
# ------------------------------------------------------------

CREATE TABLE `features` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_car` int(11) DEFAULT NULL,
  `feature_name` varchar(100) DEFAULT NULL,
  `cylinders` varchar(100) DEFAULT NULL,
  `city_mpg` varchar(100) DEFAULT NULL,
  `highway_mpg` varchar(100) DEFAULT NULL,
  `engine` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `features` WRITE;
/*!40000 ALTER TABLE `features` DISABLE KEYS */;

INSERT INTO `features` (`id`, `id_car`, `feature_name`, `cylinders`, `city_mpg`, `highway_mpg`, `engine`)
VALUES
	(1,1,'EXTERIOR','L4','20 MPG','25 MPG','1.3'),
	(2,1,'PERFORMANCE','L4','20 MPG','25 MPG','1.3'),
	(3,2,'EXTERIOR','L3','23 MPG','28 MPG','1.5'),
	(4,2,'PERFORMANCE','L3','23 MPG','28 MPG','1.5');

/*!40000 ALTER TABLE `features` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla images
# ------------------------------------------------------------

CREATE TABLE `images` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_car` int(11) DEFAULT NULL,
  `url_image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `par_ind` (`id_car`),
  CONSTRAINT `images_ibfk_1` FOREIGN KEY (`id_car`) REFERENCES `cars` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;

INSERT INTO `images` (`id`, `id_car`, `url_image`)
VALUES
	(1,1,'car1.png'),
	(2,1,'car2.png'),
	(3,1,'car3.png'),
	(4,1,'car4.png'),
	(5,1,'car5.png'),
	(6,1,'car6.png'),
	(7,2,'hummer1.png'),
	(8,2,'hummer2.png'),
	(9,2,'hummer3.png'),
	(10,2,'hummer4.png'),
	(11,2,'hummer5.png'),
	(12,2,'hummer6.png');

/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
